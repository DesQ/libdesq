# libdesq

The DesQ library to obtain system information and various classes and functions to be used across the DesQ project.
This library provides single-instance Application classes for Core and Gui, advanced file-system watcher, a very simple IPC,
functions to return XDG variables, desktop file parsing, and read various system info like battery, network, storage, cpu
and RAM info.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/libdesq.git libdesq`
- Enter the `libdesq` folder
  * `cd libdesq`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.2
* Meson:            0.60.1
* Ninja:            1.10.1

### Known Bugs
* Please test and let us know


### Upcoming
* A nice way to handle unix signals like SIGINT, SIGTERM, SIGHUP, etc from within DesQ*Application (#1)
* Pickup the default font from font-config (#3)
