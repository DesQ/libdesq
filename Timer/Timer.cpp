/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Timer.hpp"
#include <QDateTime>

DesQ::Timer::Timer( QObject *parent ) : QObject( parent ), mTimer( std::make_unique<QBasicTimer>() ) {
}


DesQ::Timer::~Timer() {
    stop();
}


void DesQ::Timer::start( int interval ) {
    QMutexLocker locker( &mMutex );

    /** Set the timer as active and not paused */
    mPaused = false;
    mActive = true;

    /** Time at which the timer was started */
    qint64 current = QDateTime::currentMSecsSinceEpoch();

    /** The time that is remaining */
    mStopTime = current + interval;

    /** Start the timer */
    mTimer->start( interval, Qt::PreciseTimer, this );
}


void DesQ::Timer::stop() {
    /** Not active */
    mActive = false;

    /** Not paused */
    mPaused = false;

    /** Remaining time is 0 */
    mRemainingTime = 0;

    /** Start time is 0 */
    mStopTime = -1;

    /** Stop the timer */
    mTimer->stop();
}


void DesQ::Timer::pause() {
    QMutexLocker locker( &mMutex );

    /** Pause only if the timer is active! */
    if ( ( mPaused == false ) && mActive ) {
        /** Set the timer to paused */
        mPaused = true;

        /** Update the remaining time */
        mRemainingTime = remainingTime();

        /** Timer is paused; stopTime is invaild */
        mStopTime = -1;

        /** Stop the timer */
        mTimer->stop();

        /** Emit the signal */
        emit paused();
    }
}


void DesQ::Timer::resume() {
    QMutexLocker locker( &mMutex );

    if ( mPaused && mActive ) {
        /** Set as not paused */
        mPaused = false;

        /** The new starting time */
        qint64 current = QDateTime::currentMSecsSinceEpoch();

        /** Update the ne stop time */
        mStopTime = current + mRemainingTime;

        /** Restart the timer */
        mTimer->start( mRemainingTime, Qt::PreciseTimer, this );

        /** Emit the resumed signal */
        emit resumed();
    }
}


uint DesQ::Timer::remainingTime() {
    /** We've already updated the remaining time when pausing */
    if ( mPaused ) {
        return mRemainingTime;
    }

    /** Quickly calculate the remaining time */
    qint64 remaining = mStopTime - QDateTime::currentMSecsSinceEpoch();

    /** Just in case, there is an integer overflow */
    return ( remaining > 0 ? static_cast<uint>( remaining ) : 0 );
}


bool DesQ::Timer::isPaused() const {
    return mPaused;
}


bool DesQ::Timer::isActive() const {
    return mActive;
}


void DesQ::Timer::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == mTimer->timerId() ) {
        /** Stop the timer, and reset */
        stop();

        /** Emit the signal */
        emit timeout();

        /** We're done */
        return;
    }

    QObject::timerEvent( tEvent );
}
