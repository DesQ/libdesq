/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Settings.hpp"
#include <DFHjsonParser.hpp>

DesQ::Settings::Settings( QString filename ): DFL::Settings( filename ) {
    /** Register a global Hjson backend only in another instance has not done it already. */
    if ( DFL::Settings::registeredBackends().contains( "HjsonFull" ) == false ) {
        DFL::Settings::registerBackend( "HjsonFull", "hjson", DFL::Config::Hjson::readConfigFromFile, DFL::Config::Hjson::writeConfigToFile );
    }
}


DesQ::Settings::Settings( QString app, QString name ): DFL::Settings( "DesQ", app, name ) {
    /** Register a global Hjson backend only in another instance has not done it already. */
    if ( DFL::Settings::registeredBackends().contains( "HjsonFull" ) == false ) {
        DFL::Settings::registerBackend( "HjsonFull", "hjson", DFL::Config::Hjson::readConfigFromFile, DFL::Config::Hjson::writeConfigToFile );
    }

    setConfigPath( DFL::Settings::SystemScope, "/usr/share/@p/configs/@A/@S.ext" );
    setConfigPath( DFL::Settings::DistroScope, "/etc/xdg/@p/@A/@S.ext" );
    setConfigPath( DFL::Settings::UserScope,   QDir::home().filePath( ".config/@P/@A/@S.ext" ) );
}


QStringList DesQ::Settings::pages() const {
    QStringList pages;

    for( QString key: allKeys() ) {
        QString page = key.split( "::" ).at( 0 );

        /** Multiple keys can be in the same page */
        if ( pages.contains( page ) == false ) {
            pages << page;
        }
    }

    return pages;
}


QStringList DesQ::Settings::groups( QString page ) const {
    QStringList groups;

    for( QString key: allKeys() ) {
        if ( key.startsWith( page + "::" ) ) {
            QString group = key.split( "::" ).at( 1 );

            /** Multiple keys can be in the same page */
            if ( groups.contains( group ) == false ) {
                groups << group;
            }
        }
    }

    return groups;
}


QStringList DesQ::Settings::keys( QString page, QString group ) const {
    QStringList keys;

    for( QString _key: allKeys() ) {
        if ( _key.startsWith( page + "::" + group + "::" ) ) {
            QString key = _key.split( "::" ).at( 2 );

            /** Multiple keys can be in the same page */
            if ( keys.contains( key ) == false ) {
                keys << key;
            }
        }
    }

    return keys;
}


QStringList DesQ::Settings::fields( QString page, QString group, QString key ) const {
    QStringList fields;

    for( QString _key: allKeys() ) {
        if ( _key.startsWith( page + "::" + group + "::" + key + "::" ) ) {
            QString field = _key.split( "::" ).at( 2 );

            /** Multiple keys can be in the same page */
            if ( fields.contains( field ) == false ) {
                fields << field;
            }
        }
    }

    return fields;
}
