/**
 * IMPL Class
 *  This class will parse the mimeapps.list files.
 *  The class structure and working is inspired by QSettings.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 */

#pragma once

#include <QtCore>

class MimeAppsListParser : public QObject {
    Q_OBJECT;

    public:
        MimeAppsListParser( QString filename );
        ~MimeAppsListParser();

        /** Get the value of a key in a section */
        QVariant value( QString section, QString key );

        /** Set the value for a key in a section */
        void setValue( QString section, QString key, QVariant value );

        /** List all the sections, alphabetically (c-is) sorted */
        QStringList sections();

        /** List all the keys of a section, alphabetically (c-is) sorted */
        QStringList keys( QString section );

        /** Save all the changes */
        void sync();

    private:
        void parse();

        QString mFileName;
        QHash<QString, QHash<QString, QVariant> > sectKeyValueMap;
        bool modified = false;
        QFileSystemWatcher *fsw;
};
