/**
 * IMPL Class
 *  This class will parse the desktop entry files.
 *  This is purely an implementational detail
 *  This file and/or functions/classes within can be modified/removed at anytime
 */

#include "XdgDesktopImpl.hpp"

typedef struct _desktop_action {
    QString id;
    QString name;
    QString icon;
    QString exec;
} Action;

QMap<QString, QVariant> parseDesktop( QString filename ) {
    QFile desktop( filename );

    if ( not desktop.open( QFile::ReadOnly ) ) {
        /** Invalid desktop file */
        return QMap<QString, QVariant>();
    }

    /**
     * Parsing
     * 1. Read line by line.
     * 2. Ignore all lines that are empty or start with #
     * 3. If the lines contains [Desktop Entry], then all info will be added
     *    to map until another [Desktop Action %s] is encountered.
     * 4. Once we encounter [Desktop Action %s] , we will create a new Action,
     *    and populate it.
     * 5. If we encounter any line starting with [, but not [Desktop Entry] or
     *    [Desktop Action %s], all lines below it will be ignored until
     *    the next time we encounter [...].
     */

    QMap<QString, QVariant> desktopMap;
    QList<Action>           actions;

    int state = 0;      /** 1 -> DE; 2 -> DA; 0 -> Intermediate */

    Action act{ "", "", "application-x-executable", "" };

    while ( not desktop.atEnd() ) {
        QString line( desktop.readLine().trimmed() );

        if ( line == "[Desktop Entry]" ) {
            state = 1;

            /** Check if act is valid. If it is, add it to actions */
            if ( not act.name.isEmpty() and not act.exec.isEmpty() ) {
                actions << act;
                act = { "", "", "", "" };
            }
        }

        else if ( line.startsWith( "[Desktop Action" ) and line.endsWith( "]" ) ) {
            state = 2;

            /** Check if act is valid. If it is, add it to actions */
            if ( not act.name.isEmpty() and not act.exec.isEmpty() ) {
                actions << act;
            }

            act    = { "", "", "", "" };
            act.id = line.replace( "[Desktop Action", "" ).replace( "]", "" ).trimmed();
        }

        /** Anything else starting with [ is not tolerated */
        else if ( line.startsWith( "[" ) ) {
            state = 0;

            /** Check if act is valid. If it is, add it to actions */
            if ( not act.name.isEmpty() and not act.exec.isEmpty() ) {
                actions << act;
            }

            act = { "", "", "", "" };
        }

        /** Comment. Ignore line. */
        else if ( line.isEmpty() ) {
            continue;
        }

        /** Comment. Ignore line. */
        else if ( line.startsWith( "#" ) ) {
            continue;
        }

        /** All Keys start with [A-Z] */
        if ( not line.contains( QRegularExpression( "[A-Z]" ) ) ) {
            continue;
        }

        /** All valid entries must be of the form below */
        else if ( not line.contains( QRegularExpression( "[a-zA-Z\\[\\]_-]+[ ]*=[ ]*.+" ) ) ) {
            continue;
        }

        else {
            /** Invalid data. Continue until state is {1, 2} */
            if ( state == 0 ) {
                continue;
            }

            QStringList split = line.split( "=", Qt::SkipEmptyParts );

            QString key   = split.takeFirst();
            QString value = split.join( "=" );

            /** Needs further investigation */
            if ( state == 1 ) {
                if ( key == "Categories" ) {
                    desktopMap[ key ] = value.split( ";", Qt::SkipEmptyParts );
                }

                else if ( key == "MimeType" ) {
                    desktopMap[ key ] = value.split( ";", Qt::SkipEmptyParts );
                }

                else if ( key == "Actions" ) {
                    desktopMap[ key ] = value.split( ";", Qt::SkipEmptyParts );
                }

                else if ( key == "OnlyShownIn" ) {
                    desktopMap[ key ] = value.split( ";", Qt::SkipEmptyParts );
                }

                else if ( key == "NotShownIn" ) {
                    desktopMap[ key ] = value.split( ";", Qt::SkipEmptyParts );
                }

                else if ( key == "Version" ) {
                    desktopMap[ key ] = value.toDouble();
                }

                else if ( key == "Terminal" ) {
                    desktopMap[ key ] = ( value.trimmed().toLower() == "true" ? true : false );
                }

                else if ( key == "NoDisplay" ) {
                    desktopMap[ key ] = ( value.trimmed().toLower() == "true" ? true : false );
                }

                else {
                    desktopMap[ key ] = value;
                }
            }

            /** State will be 2 */
            else {
                if ( key == "Name" ) {
                    act.name = value;
                }

                else if ( key == "Icon" ) {
                    act.icon = value;
                }

                else if ( key == "Exec" ) {
                    act.exec = value;
                }
            }
        }
    }
    desktop.close();

    for ( Action act: actions ) {
        desktopMap[ act.id ] = QStringList() << act.name << act.icon << act.exec;
        // desktopMap[ act.id ] = { act.name, act.icon, act.exec };
    }

    return desktopMap;
}
