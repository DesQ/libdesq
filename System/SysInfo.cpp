/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SysInfo.hpp"

#include <unistd.h>

DesQ::SystemInfo *DesQ::SystemInfo::info = nullptr;

DesQ::SystemInfo * DesQ::SystemInfo::sysInfoObject() {
    if ( info ) {
        return info;
    }

    info = new SystemInfo();
    return info;
}


QString DesQ::SystemInfo::product() const {
    return mProduct;
}


QString DesQ::SystemInfo::hostname() const {
    return mHostname;
}


QString DesQ::SystemInfo::platform() const {
    return mPlatform;
}


QString DesQ::SystemInfo::distribution() const {
    return mDistribution;
}


QString DesQ::SystemInfo::kernelVersion() const {
    return mKernelVersion;
}


QString DesQ::SystemInfo::cpuModel() const {
    return mCpuModel;
}


int DesQ::SystemInfo::cpuMaxSpeed() const {
    return mCpuSpeed;
}


int DesQ::SystemInfo::cpuCores() const {
    return mCpuCores;
}


QString DesQ::SystemInfo::username() const {
    return mUsername;
}


QString DesQ::SystemInfo::uptime() const {
    /* SysInfo */
    struct sysinfo sinfo;
    int            ret = sysinfo( &sinfo );

    /* Uptime */
    QString mUptime;

    if ( not ret ) {
        /* Number of seconds */
        int nsecs = sinfo.uptime;

        int d = nsecs / ( 24 * 3600 );
        nsecs %= ( 24 * 3600 );

        int h = nsecs / 3600;
        nsecs %= 3600;

        int m = nsecs / 60;
        nsecs %= 60;

        int s = nsecs;

        mUptime  = ( d ? QString( "%1 day%2 " ).arg( d ).arg( d > 1 ? "s" : "" ) : "" );
        mUptime += ( h ? QString( "%1 hour%2 " ).arg( h ).arg( h > 1 ? "s" : "" ) : "" );
        mUptime += ( m ? QString( "%1 minute%2 " ).arg( m ).arg( m > 1 ? "s" : "" ) : "" );
        mUptime += ( s ? QString( "%1 second%2 " ).arg( s ).arg( s > 1 ? "s" : "" ) : "" );
    }

    return mUptime;
}


QList<qint64> DesQ::SystemInfo::getProcInfo() {
    QList<qint64> info;

    QDir proc( "/proc" );

    info << proc.entryList( { "[0-9]*" }, QDir::Dirs | QDir::NoDotAndDotDot ).count();

    QFile f( "/proc/loadavg" );

    if ( f.open( QFile::ReadOnly ) ) {
        QString avg( f.readAll() );
        info << avg.split( ' ' )[ 3 ].split( '/' )[ 1 ].toInt();
        f.close();
    }

    return info;
}


QList<qint64> DesQ::SystemInfo::getCpuSpeeds() {
    QFile f( "/proc/cpuinfo" );

    f.open( QFile::ReadOnly );

    QList<qint64> speeds;

    /* Only the first CPU Info is sufficient */
    QStringList cpuInfo = QString::fromLocal8Bit( f.readAll() ).trimmed().split( "\n\n", Qt::SkipEmptyParts )[ 0 ].split( "\n" );

    for ( QString line: cpuInfo ) {
        if ( line.startsWith( "cpu MHz" ) ) {
            speeds << line.split( ":" ).takeLast().toDouble();
        }
    }

    return speeds;
}


/* Get the current cpu usage */
QList<qint64> DesQ::SystemInfo::getCpuUsage() {
    QList<qint64> usages;

    QFile statf( "/proc/stat" );

    statf.open( QFile::ReadOnly );

    QStringList lines = QString::fromLocal8Bit( statf.readAll() ).split( "\n" );

    for ( int c = 0; c <= mCpuCores; c++ ) {
        QStringList times = lines[ c ].trimmed().split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts );
        times.removeFirst();

        if ( times.count() < 8 ) {
            qDebug() << lines[ c ];
            continue;
        }

        long totalCur = 0;
        for ( int i = 0; i < 8; i++ ) {
            totalCur += times[ i ].toLong();
        }

        long idleCur = times[ 3 ].toLong() + times[ 4 ].toLong();

        long total = totalCur - lastTotals.at( c );
        long idle  = idleCur - lastIdles.at( c );

        if ( total ) {
            usages << 100 * ( total - idle ) / total;
        }

        else {
            usages << 0;
        }

        lastTotals[ c ] = totalCur;
        lastIdles[ c ]  = idleCur;
    }

    return usages;
}


/* Average CPU Loads over all CPUs */
QList<qint64> DesQ::SystemInfo::getCpuLoads() {
    struct sysinfo sinfo;

    sysinfo( &sinfo );

    QList<qint64> usage;

    usage << sinfo.loads[ 0 ] << sinfo.loads[ 1 ] << sinfo.loads[ 2 ];

    return usage;
}


/* Get the current RAM Usage */
QList<qint64> DesQ::SystemInfo::getRamUsage() {
    QFile memf( "/proc/meminfo" );

    memf.open( QFile::ReadOnly );
    QStringList mem = QString::fromLocal8Bit( memf.readAll() ).split( "\n" );

    long int total = 0, free = 0, buffer = 0, cached = 0, shared = 0, recl = 0;

    for ( QString line: mem ) {
        if ( line.startsWith( "MemTotal:" ) ) {
            total = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }

        else if ( line.startsWith( "MemFree:" ) ) {
            free = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }

        else if ( line.startsWith( "Buffers:" ) ) {
            buffer = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }

        else if ( line.startsWith( "Cached:" ) ) {
            cached = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }

        else if ( line.startsWith( "Shmem:" ) ) {
            shared = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }

        else if ( line.startsWith( "SReclaimable:" ) ) {
            recl = line.split( QRegularExpression( "\\s+" ), Qt::SkipEmptyParts ).at( 1 ).toLong();
        }
    }

    QList<qint64> usage;

    usage << ( total - ( free + buffer + ( cached + recl - shared ) ) ) * 1024 << total * 1024;

    return usage;
}


/* Get the current Swap Usage */
QList<qint64> DesQ::SystemInfo::getSwapUsage() {
    struct sysinfo sinfo;

    sysinfo( &sinfo );

    QList<qint64> usage;

    usage << ( sinfo.totalswap - sinfo.freeswap ) * sinfo.mem_unit << sinfo.totalswap * sinfo.mem_unit;

    return usage;
}


DesQ::SystemInfo::SystemInfo() {
    mHostname      = QSysInfo::machineHostName();
    mPlatform      = QString( "%1 %2" ).arg( QSysInfo::kernelType() ).arg( QSysInfo::currentCpuArchitecture() );
    mDistribution  = QSysInfo::prettyProductName();
    mKernelVersion = QSysInfo::kernelVersion();

    /* mCpuModel, mCpuSpeed, mCpuCores, mUsername, mUptime */
    readOtherInfo();
}


void DesQ::SystemInfo::readOtherInfo() {
    /* HW Info */
    QFile hw;

    hw.setFileName( "/sys/devices/virtual/dmi/id/sys_vendor" );
    hw.open( QFile::ReadOnly );
    mProduct += QString::fromLocal8Bit( hw.readAll() ) + " ";
    hw.close();

    hw.setFileName( "/sys/devices/virtual/dmi/id/product_name" );
    hw.open( QFile::ReadOnly );
    mProduct += QString::fromLocal8Bit( hw.readAll() );
    hw.close();

    hw.setFileName( "/sys/devices/virtual/dmi/id/bios_date" );
    hw.open( QFile::ReadOnly );
    mProduct += QString( " (" + hw.readAll() + ")" );
    hw.close();

    mProduct.replace( "\n", "" );

    /* CPU Model */
    QFile f( "/proc/cpuinfo" );

    f.open( QFile::ReadOnly );

    /* Only the first CPU Info is sufficient */
    QString cpuInfo = QString::fromLocal8Bit( f.readAll() ).split( "\n\n" )[ 0 ];

    QStringList infoLines = cpuInfo.split( "\n" );

    for ( QString line: infoLines ) {
        if ( line.startsWith( "model name" ) ) {
            mCpuModel = line.split( ":" )[ 1 ].trimmed();
        }
    }

    /* No. of CPU Cores */
    mCpuCores = qMax( 1, ( int )sysconf( _SC_NPROCESSORS_ONLN ) );

    /* UserName */
    mUsername = QString( getlogin() );

    /* CPU Max Speed */
    int  maxSpeed = 0;
    QDir cpus( "/sys/bus/cpu/devices/" );

    for ( QString dir: cpus.entryList( QDir::NoDotAndDotDot | QDir::Dirs ) ) {
        QFile f( cpus.filePath( dir + "/cpufreq/cpuinfo_max_freq" ) );
        f.open( QFile::ReadOnly );
        maxSpeed = qMax( maxSpeed, f.readAll().toInt() );
    }

    /* Maximum Speed of the CPU */
    mCpuSpeed = maxSpeed / 1000;

    /* lastUsed dummy values */
    for ( int i = 0; i <= mCpuCores; i++ ) {
        lastTotals << 0;
        lastIdles << 0;
    }
}
