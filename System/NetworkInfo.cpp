/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "NetworkInfo.hpp"

DesQ::NetworkInfo *DesQ::NetworkInfo::netInfo = nullptr;

DesQ::NetworkInfo *DesQ::NetworkInfo::instance() {
    if ( !netInfo ) {
        netInfo = new NetworkInfo;
    }

    return netInfo;
}


DesQ::NetworkInfo::NetworkInfo() {
    netDataPath = "/proc/net/dev";
}


QList<qint64> DesQ::NetworkInfo::getIOBytes() const {
    QList<qint64> iobytes;

    iobytes << 0 << 0;

    QFile f( netDataPath );

    if ( f.open( QFile::ReadOnly ) ) {
        QStringList data = QString( f.readAll() ).trimmed().split( "\n" );
        f.close();

        // Remove the first three lines
        data.removeFirst();
        data.removeFirst();
        data.removeFirst();

        for ( QString line: data ) {
            QStringList bytes = line.trimmed().split( QRegularExpression( "\\s+" ) );
            iobytes[ 0 ] += bytes.at( 1 ).toULongLong();
            iobytes[ 1 ] += bytes.at( 9 ).toULongLong();
        }
    }

    return iobytes;
}
