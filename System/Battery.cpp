/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Battery.hpp"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusArgument>
#include <QSettings>
#include <QDir>

BatteryManager *BatteryManager::mBatMan = nullptr;

BatteryManager * BatteryManager::instance() {
    if ( not mBatMan ) {
        mBatMan = new BatteryManager();
    }

    return mBatMan;
}


Batteries BatteryManager::batteries() {
    return mBatts;
}


bool BatteryManager::hasBatteries() {
    return mHasBatteries;
}


void BatteryManager::refreshBatteries() {
    mBatts.clear();

    QDBusInterface iface( "org.freedesktop.UPower", "/org/freedesktop/UPower", "org.freedesktop.UPower", QDBusConnection::systemBus() );
    QDBusArgument  argument = iface.call( "EnumerateDevices" ).arguments().at( 0 ).value<QDBusArgument>();

    if ( iface.lastError().type() == QDBusError::NoError ) {
        argument.beginArray();

        while ( !argument.atEnd() ) {
            QDBusObjectPath path;
            argument >> path;

            Battery batt( path.path() );

            if ( batt.isValid() ) {
                mBatts << batt;
            }
        }
    }

    mHasBatteries = ( mBatts.count() > 0 );
}


BatteryManager::BatteryManager() : QObject() {
    mHasBatteries = false;
    refreshBatteries();
}


Battery::Battery( QString path ) {
    mProperties << "NativePath" << "Path" << "Model" << "Vendor" << "Technology";
    mProperties << "PowerSupply" << "HasHistory" << "HasStatistics" << "IsPresent";
    mProperties << "IsRechargeable" << "Energy" << "EnergyEmpty" << "EnergyFull";
    mProperties << "EnergyFullDesign" << "EnergyRate" << "Voltage" << "Percentage";
    mProperties << "Capacity" << "State" << "WarningLevel" << "ToFull" << "ToEmpty";

    iface = new QDBusInterface( "org.freedesktop.UPower", path, "org.freedesktop.UPower.Device", QDBusConnection::systemBus() );
}


QString Battery::name() {
    QString bName( value( "NativePath" ).toString() );

    if ( bName.isEmpty() ) {
        bName = QFileInfo( value( "Path" ).toString() ).fileName();
    }

    return bName;
}


QStringList Battery::properties() {
    return mProperties;
}


QVariant Battery::value( QString key ) {
    if ( key == "Path" ) {
        return iface->path();
    }

    return iface->property( key.toUtf8().data() );
}


bool Battery::isValid() const {
    if ( not iface->isValid() ) {
        return false;
    }

    return ( iface->property( "Type" ).toInt() == 2 );
}
