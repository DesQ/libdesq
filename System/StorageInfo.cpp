/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QFileInfo>
#include <QIODevice>
#include <QXmlStreamReader>

#include "StorageInfo.hpp"
#include <sys/statvfs.h>

#include <desq/Utils.hpp>

#define DBUS_SERVICE       "org.freedesktop.UDisks2"
#define BLOCK_PATH         "/org/freedesktop/UDisks2/block_devices"
#define DRIVE_PATH         "/org/freedesktop/UDisks2/drives"
#define DBUS_INTROSPECT    "org.freedesktop.DBus.Introspectable"


/**
 * DesQ::StorageManager - Singleton class that manages all the DesQ::Storage volumes
 */

DesQ::StorageManager *DesQ::StorageManager::mStorageManager = nullptr;

DesQ::StorageManager *DesQ::StorageManager::instance() {
    if ( not mStorageManager ) {
        mStorageManager = new DesQ::StorageManager();
    }

    return mStorageManager;
}


DesQ::StorageDevices DesQ::StorageManager::devices() {
    return mDevices;
}


DesQ::StorageBlocks DesQ::StorageManager::blocks() {
    return mBlocks;
}


DesQ::StorageManager::StorageManager() : QObject() {
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2",
        "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager",
        "InterfacesAdded",
        this,
        SLOT( interfacesAdded( QDBusObjectPath, QMap<QString, QVariant> ) )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2",
        "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager",
        "InterfacesRemoved",
        this,
        SLOT( interfacesRemoved( QDBusObjectPath, QStringList ) )
    );

    rescanStorage();
}


DesQ::StorageBlock DesQ::StorageManager::blockForPath( QString path ) {
    rescanStorage();

    QString            largest;
    DesQ::StorageBlock blk;

    for ( DesQ::StorageBlock block: blocks() ) {
        /** Does not contain a file system - we ignore it. */
        if ( not ( block.fileSystem().length() ) ) {
            continue;
        }

        QString mtpt = block.mountPoint();

        if ( mtpt.length() ) {
            if ( path.contains( mtpt ) and ( mtpt.length() > largest.length() ) ) {
                largest = mtpt;
                blk     = block;
            }
        }
    }

    return blk;
}


void DesQ::StorageManager::rescanStorage() {
    mDrivePartsMap.clear();
    mPartDriveMap.clear();

    QDBusInterface drvIface( DBUS_SERVICE, DRIVE_PATH, DBUS_INTROSPECT, QDBusConnection::systemBus() );
    QDBusInterface devIface( DBUS_SERVICE, BLOCK_PATH, DBUS_INTROSPECT, QDBusConnection::systemBus() );

    if ( not ( drvIface.isValid() and devIface.isValid() ) ) {
        return;
    }

    QDBusReply<QString> drvRep = drvIface.call( "Introspect" );
    QDBusReply<QString> devRep = devIface.call( "Introspect" );

    if ( not ( drvRep.isValid() and devRep.isValid() ) ) {
        return;
    }

    QXmlStreamReader drvXml( drvRep.value() );
    QStringList      response;

    while ( !drvXml.atEnd() ) {
        drvXml.readNext();

        if ( ( drvXml.tokenType() == QXmlStreamReader::StartElement ) && ( drvXml.name().toString() == "node" ) ) {
            QString drive = drvXml.attributes().value( "name" ).toString();

            if ( drive.isEmpty() ) {
                continue;
            }

            if ( not mDrivePartsMap.contains( drive ) ) {
                mDrivePartsMap[ drive ] = QStringList();
            }
        }
    }

    QXmlStreamReader devXml( devRep.value() );

    while ( !devXml.atEnd() ) {
        devXml.readNext();

        if ( ( devXml.tokenType() == QXmlStreamReader::StartElement ) && ( devXml.name().toString() == "node" ) ) {
            QString blockId = devXml.attributes().value( "name" ).toString();

            if ( blockId.isEmpty() ) {
                continue;
            }

            DesQ::StorageBlock block( blockId );
            mPartDriveMap[ blockId ] = DesQ::Utils::baseName( block.drive() );
            mDrivePartsMap[ DesQ::Utils::baseName( block.drive() ) ] << blockId;
        }
    }

    mDevices.clear();
    mBlocks.clear();

    /** Don't list invalid devices */
    for ( QString drive: mDrivePartsMap.keys() ) {
        DesQ::StorageDevice dev( drive );

        if ( dev.isValid() ) {
            mDevices << dev;
        }
    }

    for ( QString blockId: mPartDriveMap.keys() ) {
        DesQ::StorageBlock block( blockId );

        if ( block.isValid() ) {
            mBlocks << block;
        }
    }
}


void DesQ::StorageManager::interfacesAdded( const QDBusObjectPath& objPath, const QMap<QString, QVariant>& ) {
    /** It's a drive */
    if ( objPath.path().contains( "/drives/" ) ) {
        QString             drvId( DesQ::Utils::baseName( objPath.path() ) );
        DesQ::StorageDevice drive( drvId );

        if ( not mDrivePartsMap.contains( drvId ) ) {
            mDrivePartsMap[ drvId ] = QStringList();
        }

        if ( drive.isValid() ) {
            mDevices << drive;
        }

        emit deviceAdded( drvId );
    }

    /** It's a block device */
    else if ( objPath.path().contains( "/block_devices/" ) ) {
        QString            blkId( DesQ::Utils::baseName( objPath.path() ) );
        DesQ::StorageBlock block( blkId );
        mPartDriveMap[ blkId ] = DesQ::Utils::baseName( block.drive() );

        if ( block.isValid() ) {
            mBlocks << block;
        }

        emit partitionAdded( blkId );
    }
}


void DesQ::StorageManager::interfacesRemoved( const QDBusObjectPath& objPath, const QStringList& ) {
    /**
     * Remove the partitions, then emit the corresponding signal.
     * If there are no more devices, emit the device removed signal.
     */

    QString id( DesQ::Utils::baseName( objPath.path() ) );

    if ( objPath.path().contains( "/drives/" ) ) {
        for ( QString blk: mDrivePartsMap.value( id ) ) {
            emit partitionRemoved( blk );
            mPartDriveMap.remove( blk );
        }

        mDrivePartsMap.remove( id );
        emit deviceRemoved( id );
    }

    else if ( objPath.path().contains( "/block_devices/" ) ) {
        QString drive = mPartDriveMap.take( id );
        emit    partitionRemoved( id );

        if ( mDrivePartsMap.contains( drive ) ) {
            QStringList blocks = mDrivePartsMap[ drive ];
            blocks.removeAll( id );

            if ( blocks.count() ) {
                mDrivePartsMap[ drive ] = blocks;
            }

            else {
                mDrivePartsMap.remove( drive );
                emit deviceRemoved( drive );
            }
        }
    }

    rescanStorage();
}


/**
 * DesQ::StorageDevice - Class to access the physical devices
 */

DesQ::StorageDevice::StorageDevice() {
    mIsValid      = false;
    mLabel        = QString();
    mPath         = QString();
    mId           = QString();
    mIsRemovable  = false;
    mIsOptical    = false;
    mSize         = 0;
    mRotationRate = 0;
    mSeat         = QString();
}


DesQ::StorageDevice::StorageDevice( QString driveID ) : mDriveId( driveID ) {
    iface = new QDBusInterface(
        DBUS_SERVICE,
        DRIVE_PATH "/" + driveID,
        QString( "%1.Drive" ).arg( DBUS_SERVICE ),
        QDBusConnection::systemBus()
    );

    if ( not iface->isValid() ) {
        return;
    }

    mLabel        = property( "Vendor" ).toString() + " " + property( "Model" ).toString();
    mPath         = iface->path();
    mId           = property( "Id" ).toString();
    mIsRemovable  = property( "Removable" ).toBool();
    mIsOptical    = ( property( "MediaCompatibility" ).toStringList().filter( "optical" ).count() > 0 );
    mSize         = property( "Size" ).toULongLong();
    mRotationRate = property( "RotationRate" ).toInt();
    mSeat         = property( "Seat" ).toString();

    mIsValid = ( mId.isEmpty() ? false : true );
}


bool DesQ::StorageDevice::isValid() {
    return ( iface->isValid() and mIsValid );
}


DesQ::StorageBlocks DesQ::StorageDevice::partitions() {
    readPartitions();

    return mParts;
}


DesQ::StorageBlocks DesQ::StorageDevice::validPartitions() {
    readPartitions();

    return mValidParts;
}


QString DesQ::StorageDevice::label() {
    return mLabel;
}


QString DesQ::StorageDevice::path() {
    return mPath;
}


QString DesQ::StorageDevice::id() {
    return mId;
}


bool DesQ::StorageDevice::isRemovable() {
    return mIsRemovable;
}


bool DesQ::StorageDevice::isOptical() {
    return mIsOptical;
}


qint64 DesQ::StorageDevice::size() {
    return mSize;
}


int DesQ::StorageDevice::rotationRate() {
    return mRotationRate;
}


QString DesQ::StorageDevice::seat() {
    return mSeat;
}


QVariant DesQ::StorageDevice::property( QString key ) {
    if ( not iface ) {
        return QVariant();
    }

    return iface->property( key.toLatin1().constData() );
}


void DesQ::StorageDevice::readPartitions() {
    mParts.clear();
    mPartNames.clear();
    mValidParts.clear();

    if ( not isValid() ) {
        return;
    }

    Q_FOREACH ( DesQ::StorageBlock block, DesQ::StorageManager::instance()->blocks() ) {
        if ( QFileInfo( block.drive() ).baseName() == mDriveId ) {
            if ( not mPartNames.contains( block.path() ) ) {
                mParts << block;
                mPartNames << block.path();

                if ( block.fileSystem().length() and block.fileSystem() != "swap" ) {
                    mValidParts << block;
                }
            }
        }
    }
}


/**
 * DesQ::StorageBlock - Class to access a block device (physical or loop or any such)
 */

DesQ::StorageBlock::StorageBlock() {
    mLabel         = QString();
    mPath          = QString();
    mDevice        = QString();
    mDrive         = QString();
    mMountPoint    = QString();
    mFileSystem    = QString();
    mIsOptical     = false;
    mIsRemovable   = false;
    mAvailableSize = 0;
    mTotalSize     = 1;
}


DesQ::StorageBlock::StorageBlock( QString id ) {
    mPath   = BLOCK_PATH "/" + id;
    mDevice = "/dev/" + id;
    getMountPoint();

    QDBusInterface blockIFace( DBUS_SERVICE, mPath, QString( "%1.Block" ).arg( DBUS_SERVICE ), QDBusConnection::systemBus() );
    QDBusInterface partIFace( DBUS_SERVICE, mPath, QString( "%1.Partition" ).arg( DBUS_SERVICE ), QDBusConnection::systemBus() );

    if ( not ( blockIFace.isValid() and partIFace.isValid() ) ) {
        return;
    }

    QString name = blockIFace.property( "IdLabel" ).toString().simplified();

    /* If we don't get the name from IdLabel */
    if ( not name.length() ) {
        name = QFileInfo( mMountPoint ).baseName();
    }

    /* If the device is not mounted */
    if ( not name.length() ) {
        name = id;
    }

    mLabel = QString( name );

    mDrive      = blockIFace.property( "Drive" ).value<QDBusObjectPath>().path();
    mFileSystem = blockIFace.property( "IdType" ).toString();

    QDBusInterface driveIFace( DBUS_SERVICE, mDrive, QString( "%1.Drive" ).arg( DBUS_SERVICE ), QDBusConnection::systemBus() );

    if ( not driveIFace.isValid() ) {
        return;
    }

    QStringList compat = driveIFace.property( "MediaCompatibility" ).toStringList();

    mIsOptical = compat.filter( "optical_" ).count() > 0;

    mIsRemovable = driveIFace.property( "Removable" ).toBool();
    mTotalSize   = partIFace.property( "Size" ).toULongLong();

    if ( not mMountPoint.isEmpty() ) {
        struct statvfs buf;

        if ( statvfs( mMountPoint.toUtf8().constData(), &buf ) == 0 ) {
            mAvailableSize = buf.f_bavail * buf.f_frsize;
        }
    }
}


bool DesQ::StorageBlock::isValid() {
    return DesQ::Utils::exists( mDevice );
}


void DesQ::StorageBlock::getMountPoint() {
    QFile dev( "/etc/mtab" );

    dev.open( QFile::ReadOnly );

    QStringList mounts = QString::fromLocal8Bit( dev.readAll() ).split( "\n", Qt::SkipEmptyParts );

    Q_FOREACH ( QString mount, mounts ) {
        if ( mount.startsWith( mDevice + " " ) ) {
            mMountPoint = mount.split( QRegularExpression( "\\s" ), Qt::SkipEmptyParts )[ 1 ];
            break;
        }
    }
}


QString DesQ::StorageBlock::label() {
    return mLabel;
}


QString DesQ::StorageBlock::path() {
    return mPath;
}


QString DesQ::StorageBlock::device() {
    return mDevice;
}


QString DesQ::StorageBlock::drive() {
    return mDrive;
}


QString DesQ::StorageBlock::mountPoint() {
    return mMountPoint;
}


QString DesQ::StorageBlock::fileSystem() {
    return mFileSystem;
}


bool DesQ::StorageBlock::isOptical() {
    return mIsOptical;
}


bool DesQ::StorageBlock::isRemovable() {
    return mIsRemovable;
}


qint64 DesQ::StorageBlock::totalSize() {
    return mTotalSize;
}


qint64 DesQ::StorageBlock::availableSize() {
    return mAvailableSize;
}


QVariant DesQ::StorageBlock::property( QString interface, QString key ) {
    QDBusInterface iface( DBUS_SERVICE, mPath, QString( DBUS_SERVICE ) + "." + interface, QDBusConnection::systemBus() );

    if ( not iface.isValid() ) {
        qCritical() << QObject::tr( "Failed D-Bus connection." );
        return QVariant();
    }

    return iface.property( key.toLocal8Bit().constData() );
}


bool DesQ::StorageBlock::mount() {
    if ( ( not mIsOptical ) and ( mFileSystem != "iso9660" ) and ( mFileSystem != "udf" ) ) {
        QDBusInterface filesystem( DBUS_SERVICE, mPath, QString( "%1.Filesystem" ).arg( DBUS_SERVICE ), QDBusConnection::systemBus() );

        if ( !filesystem.isValid() ) {
            qCritical() << QObject::tr( "Failed D-Bus connection." );
            return false;
        }

        QVariantMap options;

        if ( mFileSystem == "vfat" ) {
            options.insert( "options", "flush" );
        }

        QDBusReply<QString> mountpoint = filesystem.call( "Mount", options );
        QString             errStr( mountpoint.error().message() );

        if ( not errStr.isEmpty() ) {
            qCritical() << "[QDBus Error]" << errStr;
            return false;
        }

        getMountPoint();
        return true;
    }

    /* Mount this optical deivce */
    else {
        /* UDisks2 DBus API cannot mount an optical device ( bug ); use udisks command */
        QProcess proc;
        proc.start( QString( "udisksctl" ), QStringList() << "mount" << "-b" << mDevice );
        proc.waitForFinished();

        getMountPoint();
        return ( proc.exitCode() > 0 ? false : true );
    }
}


bool DesQ::StorageBlock::unmount() {
    if ( not mIsOptical ) {
        QDBusInterface filesystem( DBUS_SERVICE, mPath, QString( "%1.Filesystem" ).arg( DBUS_SERVICE ), QDBusConnection::systemBus() );

        if ( !filesystem.isValid() ) {
            qCritical() << QObject::tr( "Failed D-Bus connection." );
            return false;
        }

        QDBusMessage reply = filesystem.call( "Unmount", QVariantMap() );

        QString errStr( reply.errorMessage() );

        if ( not errStr.isEmpty() ) {
            qCritical() << errStr;
            return false;
        }

        mMountPoint = "";
        return true;
    }

    /* Unmount this optical device */
    else {
        /* UDisks2 DBus API cannot unmount an optical device ( bug ); use udisks command */
        QProcess proc;
        proc.start( QString( "udisksctl" ), QStringList() << "unmount" << "-b" << mDevice );
        proc.waitForFinished();

        getMountPoint();
        return ( proc.exitCode() > 0 ? false : true );
    }
}
