/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <DFSettings.hpp>
#include <DFHjsonParser.hpp>

#include "Utils.hpp"


QVariantMap readSimpleSettings( QString filename ) {
    QVariantMap cfg = DFL::Config::Hjson::readConfigFromFile( filename );

    QVariantMap map;

    /** Top-level keys are pages */
    for ( QString page: cfg.keys() ) {
        QVariantMap pMap = cfg[ page ].toMap();

        /** The keys of a page will give us groups */
        for ( QString group: pMap.keys() ) {
            QVariantMap gMap = pMap[ group ].toMap();

            /** The keys of a group will give us fields */
            for ( QString field: gMap.keys() ) {
                QVariantMap fMap = gMap[ field ].toMap();

                /** Proceed only if this field has a key that stores a config value */
                if ( fMap.contains( "value" ) ) {
                    map[ page + "::" + group + "::" + field ] = fMap[ "value" ];
                }
            }
        }
    }

    return map;
}


bool writeSimpleSettings( QVariantMap settingMap, QString filename ) {
    /** Top-level keys are pages */
    for ( QString page: settingMap.keys() ) {
        QVariantMap pMap = settingMap[ page ].toMap();

        /** The keys of a page will give us groups */
        for ( QString group: pMap.keys() ) {
            QVariantMap gMap = pMap[ group ].toMap();

            /** The keys of a group will give us fields */
            for ( QString field: gMap.keys() ) {
                QVariantMap fMap = gMap[ field ].toMap();
                fMap[ "value" ] = gMap[ field ];

                gMap[ field ] = fMap;
            }

            pMap[ group ] = gMap;
        }

        settingMap[ page ] = pMap;
    }

    return DFL::Config::Hjson::writeConfigToFile( settingMap, filename );
}


DFL::Settings * DesQ::Utils::initializeDesQSettings( QString app, QString file, bool hjson ) {
    if ( DFL::Settings::registeredBackends().contains( "Hjson" ) == false ) {
        DFL::Settings::registerBackend( "Hjson", "hjson", readSimpleSettings, writeSimpleSettings );
    }

    DFL::Settings *sett = new DFL::Settings( "DesQ", app, file );

    sett->setConfigPath( DFL::Settings::SystemScope, "/usr/share/@p/configs/@S.ext" );
    sett->setConfigPath( DFL::Settings::DistroScope, "/etc/xdg/@p/@S.ext" );
    sett->setConfigPath( DFL::Settings::UserScope,   QDir::home().filePath( ".config/@P/@S.ext" ) );

    if ( hjson ) {
        sett->setBackend( "Hjson" );
    }

    return sett;
}


DFL::Settings * DesQ::Utils::initializeDesQPluginSettings( QString app, QString plugin, bool hjson ) {
    if ( DFL::Settings::registeredBackends().contains( "Hjson" ) == false ) {
        DFL::Settings::registerBackend( "Hjson", "hjson", readSimpleSettings, writeSimpleSettings );
    }

    DFL::Settings *sett = new DFL::Settings( "DesQ", app, plugin );

    sett->setConfigPath( DFL::Settings::SystemScope, "/usr/share/@p/configs/@A/@S.ext" );
    sett->setConfigPath( DFL::Settings::DistroScope, "/etc/xdg/@p/@A/@S.ext" );
    sett->setConfigPath( DFL::Settings::UserScope,   QDir::home().filePath( ".config/@P/@A/@S.ext" ) );

    if ( hjson ) {
        sett->setBackend( "Hjson" );
    }

    return sett;
}
