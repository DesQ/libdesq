/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <stdio.h>
#include <stdlib.h>

#include "Utils.hpp"
#include "desq-config.h"

static QMimeDatabase mimeDb;

QString DesQ::Utils::dirName( QString path ) {
    if ( path == "/" or path == "//" ) {
        return "/";
    }

    /* Simple clean path: remove '//' and './' */
    path = path.replace( "//", "/" ).replace( "/./", "/" );

    char    *dupPath = strdup( path.toLocal8Bit().constData() );
    QString dirPth   = QString::fromLocal8Bit( dirname( dupPath ) );

    dirPth += ( dirPth.endsWith( "/" ) ? "" : "/" );
    free( dupPath );

    return dirPth;
}


QString DesQ::Utils::baseName( QString path ) {
    if ( path == "/" or path == "//" ) {
        return "/";
    }

    /* Simple clean path" remove '//' and './' */
    path = path.replace( "//", "/" ).replace( "/./", "/" );

    char    *dupPath = strdup( path.toLocal8Bit().constData() );
    QString basePth  = QString::fromLocal8Bit( basename( dupPath ) );

    free( dupPath );

    return basePth;
}


QString DesQ::Utils::getMimeType( QString path ) {
    return mimeDb.mimeTypeForFile( path ).name();
}


QString DesQ::Utils::getMimeIcon( QString path ) {
    return mimeDb.mimeTypeForFile( path ).iconName();
}


bool DesQ::Utils::isFile( QString path ) {
    struct stat statbuf;

    if ( stat( path.toLocal8Bit().constData(), &statbuf ) == 0 ) {
        if ( S_ISREG( statbuf.st_mode ) or S_ISLNK( statbuf.st_mode ) ) {
            return true;
        }

        else {
            return false;
        }
    }

    else {
        return false;
    }
}


bool DesQ::Utils::isDir( QString path ) {
    struct stat statbuf;

    if ( stat( path.toLocal8Bit().constData(), &statbuf ) == 0 ) {
        if ( S_ISDIR( statbuf.st_mode ) ) {
            return true;
        }

        else {
            return false;
        }
    }

    else {
        return false;
    }
}


bool DesQ::Utils::isLink( QString path ) {
    struct stat statbuf;

    if ( lstat( path.toLocal8Bit().constData(), &statbuf ) == 0 ) {
        if ( S_ISLNK( statbuf.st_mode ) ) {
            return true;
        }

        else {
            return false;
        }
    }

    else {
        return false;
    }
}


bool DesQ::Utils::exists( QString path ) {
    return not access( path.toLocal8Bit().constData(), F_OK );
}


QString DesQ::Utils::readLink( QString path ) {
    char linkTarget[ PATH_MAX ] = { 0 };

    realpath( path.toLocal8Bit().constData(), linkTarget );

    return QString( linkTarget );
}


int DesQ::Utils::mkpath( QString path, mode_t mode ) {
    /* Root always exists */
    if ( path == "/" ) {
        return 0;
    }

    /* If the directory exists, thats okay for us */
    if ( DesQ::Utils::exists( path ) ) {
        return 0;
    }

    mkpath( DesQ::Utils::dirName( path ), mode );

    return mkdir( path.toLocal8Bit().constData(), mode );
}


bool DesQ::Utils::removeDir( QString dirName ) {
    bool result = true;
    QDir dir( dirName );

    QDirIterator it( dirName, QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files );

    while ( it.hasNext() ) {
        it.next();

        if ( it.fileInfo().isDir() ) {
            result &= DesQ::Utils::removeDir( it.filePath() );
        }

        else {
            result &= QFile::remove( it.filePath() );
        }
    }

    rmdir( dirName.toUtf8().constData() );

    return result;
}


bool DesQ::Utils::isReadable( QString path ) {
    if ( isDir( path ) ) {
        return not access( path.toLocal8Bit().constData(), R_OK | X_OK );
    }

    else {
        return not access( path.toLocal8Bit().constData(), R_OK );
    }
}


bool DesQ::Utils::isWritable( QString path ) {
    return not access( path.toLocal8Bit().constData(), W_OK );
}


bool DesQ::Utils::isExecutable( QString path ) {
    struct stat statbuf;

    if ( stat( path.toLocal8Bit().data(), &statbuf ) != 0 ) {
        return false;
    }

    if ( ( statbuf.st_mode & S_IXUSR ) ) {
        QMimeType m = mimeDb.mimeTypeForFile( path );

        if ( m.name() == "application/x-executable" ) {
            return true;
        }

        else if ( m.name() == "application/x-sharedlib" ) {
            return true;
        }

        else if ( m.allAncestors().contains( "application/x-executable" ) ) {
            return true;
        }

        /* Default is false */
        return false;
    }

    return false;
}


qint64 DesQ::Utils::getSize( QString path ) {
    struct stat statbuf;

    if ( stat( path.toLocal8Bit().constData(), &statbuf ) != 0 ) {
        return 0;
    }

    switch ( statbuf.st_mode & S_IFMT ) {
        case S_IFREG: {
            return statbuf.st_size;
        }

        case S_IFDIR: {
            DIR           *d_fh;
            struct dirent *entry;
            QString       longest_name;

            while ( ( d_fh = opendir( path.toLocal8Bit().constData() ) ) == NULL ) {
                qWarning() << "Couldn't open directory:" << path;
                return statbuf.st_size;
            }

            qint64 size = statbuf.st_size;

            longest_name = QString( path );

            if ( not longest_name.endsWith( "/" ) ) {
                longest_name += "/";
            }

            while ( ( entry = readdir( d_fh ) ) != NULL ) {
                /* Don't ascend up the tree or include the current directory */
                if ( ( strcmp( entry->d_name, ".." ) != 0 ) && ( strcmp( entry->d_name, "." ) != 0 ) ) {
                    if ( entry->d_type == DT_DIR ) {
                        /* Recurse into that folder */
                        size += getSize( longest_name + entry->d_name );
                    }

                    else {
                        /* Get the size of the current file */
                        size += getSize( longest_name + entry->d_name );
                    }
                }
            }

            closedir( d_fh );
            return size;
        }

        default: {
            /* Return 0 for all other nodes: chr, blk, lnk, symlink etc */
            return 0;
        }
    }

    /* Should never come till here */
    return 0;
}


mode_t DesQ::Utils::getMode( QString path ) {
    struct stat fileAtts;

    if ( stat( path.toLocal8Bit().constData(), &fileAtts ) != 0 ) {
        return -1;
    }

    return fileAtts.st_mode;
}


QStringList DesQ::Utils::recDirWalk( QString path ) {
    QStringList fileList;

    if ( not QFileInfo( path ).exists() ) {
        return QStringList();
    }

    QDirIterator it( path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::Subdirectories );

    while ( it.hasNext() ) {
        it.next();

        if ( it.fileInfo().isDir() ) {
            fileList.append( it.fileInfo().filePath() );
        }

        else {
            fileList.append( it.fileInfo().filePath() );
        }
    }

    return fileList;
}


QString DesQ::Utils::formatSize( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return QString( "%1 TiB" ).arg( QString::number( qreal( size ) / SizeHint::TiB, 'f', 3 ) );
        }
        else if ( size >= SizeHint::GiB ) {
            return QString( "%1 GiB" ).arg( QString::number( qreal( size ) / SizeHint::GiB, 'f', 2 ) );
        }
        else if ( size >= SizeHint::MiB ) {
            return QString( "%1 MiB" ).arg( QString::number( qreal( size ) / SizeHint::MiB, 'f', 1 ) );
        }
        else if ( size >= SizeHint::KiB ) {
            return QString( "%1 KiB" ).arg( QString::number( qreal( size ) / SizeHint::KiB, 'f', 1 ) );
        }
        else {
            return QString( "%1 B" ).arg( size );
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return QString( "%1 TiB" ).arg( QString::number( qreal( size ) / SizeHint::TiB, 'f', 3 ) );
        }
        else if ( hint == SizeHint::GiB ) {
            return QString( "%1 GiB" ).arg( QString::number( qreal( size ) / SizeHint::GiB, 'f', 2 ) );
        }
        else if ( hint == SizeHint::MiB ) {
            return QString( "%1 MiB" ).arg( QString::number( qreal( size ) / SizeHint::MiB, 'f', 1 ) );
        }
        else if ( hint == SizeHint::KiB ) {
            return QString( "%1 KiB" ).arg( QString::number( qreal( size ) / SizeHint::KiB, 'f', 1 ) );
        }
        else {
            return QString( "%1 B" ).arg( size );
        }
    }

    return QString();
}


double DesQ::Utils::formatSizeRaw( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return qreal( size ) / SizeHint::TiB;
        }
        else if ( size >= SizeHint::GiB ) {
            return qreal( size ) / SizeHint::GiB;
        }
        else if ( size >= SizeHint::MiB ) {
            return qreal( size ) / SizeHint::MiB;
        }
        else if ( size >= SizeHint::KiB ) {
            return qreal( size ) / SizeHint::KiB;
        }
        else {
            return size;
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return qreal( size ) / SizeHint::TiB;
        }
        else if ( hint == SizeHint::GiB ) {
            return qreal( size ) / SizeHint::GiB;
        }
        else if ( hint == SizeHint::MiB ) {
            return qreal( size ) / SizeHint::MiB;
        }
        else if ( hint == SizeHint::KiB ) {
            return qreal( size ) / SizeHint::KiB;
        }
        else {
            return size;
        }
    }

    return size;
}


QString DesQ::Utils::formatSizeStr( qint64 size, qint64 hint ) {
    if ( hint == SizeHint::Auto ) {
        if ( size >= SizeHint::TiB ) {
            return QString( "TiB" );
        }
        else if ( size >= SizeHint::GiB ) {
            return QString( "GiB" );
        }
        else if ( size >= SizeHint::MiB ) {
            return QString( "MiB" );
        }
        else if ( size >= SizeHint::KiB ) {
            return QString( "KiB" );
        }
        else {
            return QString( "B" );
        }
    }

    else {
        if ( hint == SizeHint::TiB ) {
            return QString( "TiB" );
        }
        else if ( hint == SizeHint::GiB ) {
            return QString( "GiB" );
        }
        else if ( hint == SizeHint::MiB ) {
            return QString( "MiB" );
        }
        else if ( hint == SizeHint::KiB ) {
            return QString( "KiB" );
        }
        else {
            return QString( "B" );
        }
    }

    return "B";
}


QString DesQ::Utils::getUtilityPath( QString utility ) {
    /** For example: DesQ Volume is found in /usr/bin/desq-volume */
    if ( utility.startsWith( "/" ) and exists( utility ) ) {
        return utility;
    }

    /** In all other cases, assume that it's in UtilsPath defined in desq-config.h */
    return UtilsPath "desq-" + utility;
}


DesQ::Environment DesQ::Utils::getNetworkProxy() {
    Environment proxy;

    QSettings netSettDef( ConfigPath "Network.conf", QSettings::IniFormat );
    QSettings netSett( "DesQ", "Network" );

    int proxyType = netSett.value( "ProxyType" ).toInt();

    /** Auto */
    if ( proxyType == 1 ) {
        QString httpProxy  = qgetenv( "http_proxy" );
        QString httpsProxy = qgetenv( "https_proxy" );
        QString noProxy    = qgetenv( "no_proxy" );

        QString HttpProxy  = qgetenv( "HTTP_PROXY" );
        QString HttpsProxy = qgetenv( "HTTPS_PROXY" );
        QString NoProxy    = qgetenv( "NO_PROXY" );

        /**
         * http_proxy: Use http_proxy if set, otherwise HTTP_PROXY (does not matter if it's set or not)
         * HTTP_PROXY: Use HTTP_PROXY if set, otherwise http_proxy (does not matter if it's set or not)
         */
        proxy[ "http_proxy" ] = ( httpProxy.isEmpty() ? HttpProxy : httpProxy );
        proxy[ "HTTP_PROXY" ] = ( HttpProxy.isEmpty() ? httpProxy : HttpProxy );

        /**
         * https_proxy: Use https_proxy if set, otherwise HTTPS_PROXY (does not matter if it's set or not)
         * HTTPS_PROXY: Use HTTPS_PROXY if set, otherwise https_proxy (does not matter if it's set or not)
         */
        proxy[ "https_proxy" ] = ( httpsProxy.isEmpty() ? ( HttpsProxy.isEmpty() ? proxy[ "http_proxy" ] : HttpsProxy ) : httpsProxy );
        proxy[ "HTTPS_PROXY" ] = ( HttpsProxy.isEmpty() ? ( httpsProxy.isEmpty() ? proxy[ "HTTP_PROXY" ] : httpsProxy ) : HttpsProxy );

        /**
         * no_proxy: Use no_proxy if set, otherwise NO_PROXY (does not matter if it's set or not)
         * NO_PROXY: Use NO_PROXY if set, otherwise no_proxy (does not matter if it's set or not)
         */
        proxy[ "no_proxy" ] = ( noProxy.isEmpty() ? NoProxy : noProxy );
        proxy[ "NO_PROXY" ] = ( NoProxy.isEmpty() ? noProxy : NoProxy );
    }

    /** Manual */
    else if ( proxyType == 4 ) {
        QString httpProxy  = netSett.value( "HTTPProxy" ).toString();
        QString httpsProxy = netSett.value( "HTTPSProxy" ).toString();
        QString noProxy    = netSett.value( "SkipProxy" ).toStringList().join( "," );

        proxy[ "http_proxy" ] = httpProxy;
        proxy[ "HTTP_PROXY" ] = httpProxy;

        /** Use the same value in HTTPProxy */
        if ( httpsProxy == "HTTPProxy" ) {
            proxy[ "https_proxy" ] = httpProxy;
            proxy[ "HTTPs_PROXY" ] = httpProxy;
        }

        else {
            proxy[ "https_proxy" ] = httpsProxy;
            proxy[ "HTTPS_proxy" ] = httpsProxy;
        }

        proxy[ "no_proxy" ] = noProxy;
        proxy[ "NO_PROXY" ] = noProxy;
    }

    return proxy;
}


void DesQ::Utils::prepareSessionVariables() {
    /*
     * $XDG_RUNTIME_DIR/DesQ-Session$XDG_SESSION_ID/
     * This ensures that all the sockets will be in our
     * Session folder. So multiple logins of the same user
     * will be isolated.
     */

    /** Get the $XDG_RUNTIME_DIR. By now it should be available. */
    QString desqDir( "%1/%2/%3" );
    QString workDir = qgetenv( "XDG_RUNTIME_DIR" );
    QString xSessID = qgetenv( "XDG_SESSION_ID" );
    QString orgName = qApp->organizationName();

    if ( workDir.isEmpty() ) {
        workDir = "/tmp";
        qputenv( "XDG_RUNTIME_DIR", workDir.toUtf8() );
    }

    desqDir = desqDir.arg( workDir ).arg( orgName.replace( " ", "" ) ).arg( xSessID );

    mkpath( desqDir, 0700 );
    qputenv( "__DESQ_WORK_DIR", desqDir.toUtf8() );

    /** Temp workspace for current DesQ Session */
    qInfo() << "DesQ Session work directory:" << desqDir;
}
