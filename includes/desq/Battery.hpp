/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/


#pragma once

#include "desq/Globals.hpp"
#include <QDBusConnection>

class Battery;
typedef QList<Battery> Batteries;

class BatteryManager : public QObject {
    Q_OBJECT

    public:
        static BatteryManager *instance();
        Batteries batteries();

        bool hasBatteries();
        void refreshBatteries();

    private:
        BatteryManager();

        Batteries mBatts;
        bool mHasBatteries;

        static BatteryManager *mBatMan;
};

class Battery {
    public:
        Battery( QString );

        QString name();

        QStringList properties();
        QVariant value( QString );

        bool isValid() const;

    private:
        QStringList mProperties;
        QDBusInterface *iface;
};
