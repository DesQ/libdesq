/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtDBus>
#include <QFileSystemWatcher>

#include "desq/Globals.hpp"

namespace DesQ {
    class StorageManager;

    class StorageBlock;
    typedef QList<StorageBlock>    StorageBlocks;

    class StorageDevice;
    typedef QList<StorageDevice>   StorageDevices;
}

class DesQ::StorageManager : public QObject {
    Q_OBJECT

    public:
        /* Hook to access global instance */
        static StorageManager *instance();

        /* List all the available devices */
        DesQ::StorageDevices devices();

        /* All the available blocks: will have containers and whole disks and partitions */
        DesQ::StorageBlocks blocks();

        /* Device for a path */
        DesQ::StorageBlock blockForPath( QString );

    private Q_SLOTS:
        /* Handle adding of new drives and partitions */
        void interfacesAdded( const QDBusObjectPath&, const QMap<QString, QVariant>& );

        /* Handle removing of new drives and partitions */
        void interfacesRemoved( const QDBusObjectPath&, const QStringList& );

    private:
        /* Initialization */
        StorageManager();

        /* Rescan all the available devices */
        void rescanStorage();

        /* List of all available devices */
        DesQ::StorageDevices mDevices;

        /* List of all available blocks */
        DesQ::StorageBlocks mBlocks;

        /* A map of drives and it's partitions */
        QMap<QString, QStringList> mDrivePartsMap;

        /* A map of partiton and it's drive */
        QMap<QString, QString> mPartDriveMap;

        /* Static global instance */
        static DesQ::StorageManager *mStorageManager;

    Q_SIGNALS:
        /* Something was added to /org/freedesktop/UDisks2/drives */
        void deviceAdded( QString );

        /* Something was added to /org/freedesktop/UDisks2/drives */
        void deviceRemoved( QString );

        /* Something was added to /org/freedesktop/UDisks2/block_devices */
        void partitionAdded( QString );

        /* Something was added to /org/freedesktop/UDisks2/block_devices */
        void partitionRemoved( QString );
};

class DesQ::StorageDevice {
    public:
        StorageDevice();
        StorageDevice( QString );

        bool isValid();

        DesQ::StorageBlocks partitions();
        DesQ::StorageBlocks validPartitions();

        QString label();
        QString path();
        QString id();
        bool isRemovable();
        bool isOptical();
        qint64 size();
        int rotationRate();
        QString seat();

        QVariant property( QString key );

    private:
        void readPartitions();

        QStringList mPartNames;
        DesQ::StorageBlocks mParts;
        DesQ::StorageBlocks mValidParts;
        QDBusInterface *iface = nullptr;

        QString mDriveId;

        QString mLabel;
        QString mPath;
        QString mId;
        bool mIsRemovable;
        bool mIsOptical;
        qint64 mSize;
        int mRotationRate;
        QString mSeat;

        bool mIsValid;
};

class DesQ::StorageBlock {
    public:
        StorageBlock();
        StorageBlock( QString );

        bool isValid();

        QString label();
        QString path();
        QString device();
        QString drive();
        QString mountPoint();
        QString fileSystem();

        bool isOptical();
        bool isRemovable();

        qint64 availableSize();
        qint64 totalSize();

        QVariant property( QString interface, QString key );

        bool mount();
        bool unmount();

    private:
        void getMountPoint();

        QString mLabel;
        QString mPath;
        QString mDevice;
        QString mDrive;
        QString mMountPoint;
        QString mFileSystem;

        bool mIsOptical;
        bool mIsRemovable;

        qint64 mAvailableSize;
        qint64 mTotalSize;
};
