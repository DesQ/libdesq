/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QBasicTimer>
#include <QTimerEvent>

namespace DesQ {
    class Timer;
}

class DesQ::Timer : public QObject {
    Q_OBJECT;

    public:
        explicit Timer( QObject *parent = nullptr );
        ~Timer();

        /** Reset the timer state, when an interval is specified */
        void start( int interval );

        /** Reset the timer state */
        void stop();

        /** Pause the timer */
        void pause();

        /** Resume the timer */
        void resume();

        /** Remaining time on the timer */
        uint remainingTime();

        /** Check if the timer is paused */
        bool isPaused() const;

        /** Check if the timer is active */
        bool isActive() const;

    protected:
        void timerEvent( QTimerEvent *tEvent );

    Q_SIGNALS:
        void paused();
        void resumed();
        void timeout();

    private:
        /** Flag to set single shot */
        bool mIsSingleShot = false;

        /** Flags to set active/paused */
        bool mPaused = false;
        bool mActive = false;

        /** Internal variable: time at which QBasicTimer should be stopped */
        qint64 mStopTime = 0;

        /** Internal variable: how much more time is remaining */
        qint64 mRemainingTime = 0;

        /** Internal variable: timer that does most of the work */
        std::unique_ptr<QBasicTimer> mTimer = nullptr;

        /** Timer mutex */
        mutable QMutex mMutex;
};
