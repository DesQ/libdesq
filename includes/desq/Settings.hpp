/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Utils.hpp"
#include <QtCore>
#include <QtGui>

#include <DFSettings.hpp>

namespace DFL {
    namespace Config {
        class Hjson;
    }
}

namespace DesQ {
    class Settings;
}

/**
 * This class is to handle Hjson-based settings built for use with DesQ and it's components.
 * The typical format of the settings is as follows:
 *  {
 *      Page1: {
 *          _PageName_: Name of the page
 *          Group1: {
 *              _GroupName_: Name of the group
 *              Key1: {
 *                  value: <value111>
 *                  name: ....
 *                  description: ....
 *                  type: ....
 *              }
 *          }
 *      }
 *      _Page2: {
 *          _PageName_: Name of the page
 *          _Group1: {
 *              _GroupName_: Name of the group
 *              Key1: {
 *                  value: <value211>
 *                  name: ....
 *                  description: ....
 *                  type: ....
 *              }
 *          }
 *      }
 *  }
 *
 * All settings will be in this format.
 * If there is a single page in the settings of an app, the tab bar will be hidden.
 *
 * DesQ::Settings provides two types of settings:
 *  1. Simple Settings - Simplified (key, value) pairs for use with apps.
 *  2. Full Settings   - Full complement of (key, value) pair for use in a settings modules.
 * There is no write support for full settings, since only modified values will be written
 * to the user config files.
 *
 * If any page name begins with an underscore (_), then the page name will not be
 * appended to the key in simple settings. Similarly, if any group name begins with
 * an underscore (_), then the group name will not be appended to the key in settings.
 * Furthermore, in simple settings, only the value corresponding to a single key, namely,
 * PageX::GroupY::KeyZ::value will be made available as (PageX::GroupY::KeyZ, <valueXYZ>)
 * pair.
 *
 * In the above example, only two (key, value) pairs will be made available via simple
 * settings.
 *   1. (Page1::Group1::Key1, value111)
 *   2. (Key1, value211)
 *
 * Great caution should be practiced when prefixing an underscore to page/group names.
 * If both _page1::_group1 and _page2::group2 have the same Key names Key1, it is
 * unresolved which (Key1, value??) pair will be created.
 */
class DesQ::Settings : public DFL::Settings {
    Q_OBJECT;

    public:

        /**
         * Create a unscoped settings instance.
         * For more details, refer the documentation of
         * DFL::Settings.
         */
        Settings( QString filename );

        /**
         * Create a scoped settings instance.
         * For more details, refer the documentation of DFL::Settings.
         */
        Settings( QString app, QString name );

        /** List of all pages */
        QStringList pages() const;

        /** List of all groups of a given page */
        QStringList groups( QString page ) const;

        /** List of all the keys of a given group in a given page */
        QStringList keys( QString page, QString group ) const;

        /** List of all the fields of a key of a given group in a given page */
        QStringList fields( QString page, QString group, QString key ) const;

    private:

        /**
         * Internally, we will be using the DFL's Hjson Parser.
         * This will provide us with the QVariantMap, which can be simplified
         * and used in readSimpleSettings and writeSimpleSettings functions.
         */
        DFL::Config::Hjson *hjSett = nullptr;
};
