/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// Qt Headers
#include <QtCore>
#include <libgen.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <QMimeDatabase>

namespace DFL {
    class Settings;
}

namespace DesQ {
    typedef QMap<QString, QString> Environment;

    namespace Utils {
        namespace SizeHint {
            const qint64 Auto = 0;
            const qint64 KiB  = 1024;
            const qint64 MiB  = 1024 * KiB;
            const qint64 GiB  = 1024 * MiB;
            const qint64 TiB  = 1024 * GiB;
        }

        /* Qt Equivalent of linux dirname and basename */
        QString dirName( QString path );
        QString baseName( QString path );

        /* MimeType and MimeIcon from QMimeDatabase */
        QString getMimeType( QString path );
        QString getMimeIcon( QString path );

        /* Node recognition tools */
        bool isFile( QString path );
        bool isDir( QString path );
        bool isLink( QString path );
        bool exists( QString path );

        /* Qt4 version of linux readlink(...) */
        QString readLink( QString path );

        /*
         * Create a directory: mkdir -p
         * http://fossies.org/linux/inadyn/libite/makepath.c
         */
        int mkpath( QString, mode_t mode = 0755 );

        /* Remove a directory and its contents */
        bool removeDir( QString );

        /* Is the path readable */
        bool isReadable( QString path );
        bool isWritable( QString path );
        bool isExecutable( QString path );

        /* Get the size of a directory and file */
        qint64 getSize( QString path );

        /* Mode of a file/folder */
        mode_t getMode( QString path );

        /* Get all the files and subdirs in directory */
        QStringList recDirWalk( QString );

        /* Convert the numberic size to human readable string */
        QString formatSize( qint64, qint64 hint    = SizeHint::Auto );
        QString formatSizeStr( qint64, qint64 hint = SizeHint::Auto );
        double  formatSizeRaw( qint64, qint64 hint = SizeHint::Auto );

        /** Get the path of a DesQ Utility */
        QString getUtilityPath( QString utility );

        /** Functions that prepare the settings backend for use with DFL::Settings */
        DFL::Settings * initializeDesQSettings( QString app, QString file, bool hjson       = false );
        DFL::Settings * initializeDesQPluginSettings( QString app, QString file, bool hjson = false );

        /** Proxy settings */
        DesQ::Environment getNetworkProxy();

        /** To run our apps in standalone mode. */
        void prepareSessionVariables();
    }
}
