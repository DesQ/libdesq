/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QHash>
#include <QThread>
#include <QBasicTimer>

namespace DesQ {
    class FSWatcher;
}

class DesQ::FSWatcher : public QThread {
    Q_OBJECT;

    public:

        /**
         * How should we be watching the added paths
         */
        enum Mode {
            /** Watch the directories recursively
             * So, a folder, it's contents, and subfolders and their contents
             * will be watched.
             * Any newly added files/folders will also be watched
             * WARNING: Use with extreme care!
             */
            Recursive = 0x2A16C7,

            /** Watch the directory and it's contents only
             * Don't watch the events of the subfolders. However, addition and
             * deletion of subfolders will be reported.
             * All the newly added files will be watched for all events
             */
            Contents,

            /** Watch the added path only
             * Watch the added path only. Newly created paths (files/folders)
             * will not be added to the watch automatically. However, they can
             * be added manually. Note that file/folder creation/deletion events
             * inside a watched directory will be reported.
             */
            PathOnly,
        };

        FSWatcher();
        ~FSWatcher();

        void addWatch( QString, Mode watchMode = PathOnly );
        void removeWatch( QString );

        void startWatch();
        void restartWatch();
        void stopWatch();

    protected:
        void timerEvent( QTimerEvent * ) override;

    private:
        void run();

        int inotifyFD = -1;

        /**
         * Hash to store the wd and path of the added watches.
         * inotify sorts the events based on wd.
         */
        QHash<int, QString> wdPathHash;

        /**
         * Hash to store the wd and mode of the added watches.
         * inotify sorts the events based on wd. Based on the
         * mode we will handle newly added nodes.
         */
        QHash<int, Mode> wdModeHash;

        /**
         * Cookie hashes to handle IN_MOVED_FROM/IN_MOVED_TO events.
         * We will store the cookie, and the IN_MOVED_FROM path in one hash
         * and the cookie and received time in another hash.
         * Once we receive the IN_MOVED_TO event with the same cookie,
         * We can emit a rename event. If we do not get a IN_MOVED_TO
         * event, in 500ms, we will consider it as deleted.
         */
        QHash<uint32_t, QString> cookiePathHash;
        QHash<uint32_t, time_t> cookieTimeHash;

        /**
         * Flag to stop all the watches
         */
        bool __stopWatch = false;

        /**
         * Timer to discard the IN_MOVED_FROM events when they expire.
         */
        QBasicTimer *expireTimer;

        /**
         * A list of string to
         */
        QHash<int, Mode> pendingRenames;

    Q_SIGNALS:
        void nodeCreated( QString );
        void nodeDeleted( QString );
        void nodeChanged( QString );

        void nodeRenamed( QString /* old */, QString /* new */ );

        void watchFailed( QString );

        void inotifyFailed();
};
