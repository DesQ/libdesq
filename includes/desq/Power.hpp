/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This file has been taken from libcys. Suitable modifications have
 * been made for use with DesQ.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "desq/Globals.hpp"
#include <QtDBus>

typedef QList<QDBusInterface *> PowerProviders;

class Power : public QObject {
    Q_OBJECT

    public:
        Power( QObject *parent = nullptr );
        ~Power();

        bool systemCanSuspend();
        bool systemCanHibernate();
        bool systemCanReboot();
        bool systemCanHalt();

    private:
        void createPowerProviders();

        PowerProviders providers;

    public Q_SLOTS:
        bool systemSuspend();
        bool systemHibernate();
        bool systemReboot();
        bool systemHalt();
};
