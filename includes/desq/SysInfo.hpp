/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ (https://gitlab.com/DesQ/libdesq)
 * This library contains various  Core and Gui classes which are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

/* For QString, QThread, QTimer, etc.. */
#include "desq/Globals.hpp"

/* Give No(CPUs), SysInfo - 1/5/15 min cpu load avg, ram/swap info, */
#include <sys/sysinfo.h>

namespace DesQ {
    class SystemInfo;
}

class DesQ::SystemInfo {
    public:
        static SystemInfo *sysInfoObject();

        QString product() const;

        QString hostname() const;
        QString platform() const;
        QString distribution() const;
        QString kernelVersion() const;
        QString cpuModel() const;
        int cpuMaxSpeed() const;
        int cpuCores() const;
        QString username() const;
        QString uptime() const;

        /* Get the total number of processes and threads */
        QList<qint64> getProcInfo();

        /* Get the current CPU Frequencies of each core */
        QList<qint64> getCpuSpeeds();

        /* Get the current cpu usage of each core */
        QList<qint64> getCpuUsage();

        /* Average CPU Loads over all CPUs: 1m, 5m, 15m */
        QList<qint64> getCpuLoads();

        /* Get the current RAM Usage */
        QList<qint64> getRamUsage();

        /* Get the current Swap Usage */
        QList<qint64> getSwapUsage();

    private:
        SystemInfo();

        void readOtherInfo();

        QString mProduct;           // Ex. Dell Inc. Inspiron 3451
        QString mHostname;          // Ex. MrTux
        QString mPlatform;          // Ex. Linux/GNU
        QString mDistribution;      // Ex. Debian/Sid
        QString mKernelVersion;     // Ex. 5.10
        QString mCpuModel;          // Ex. AMD A6-6310 APU with AMD Radeon R4 Graphics
        int mCpuSpeed;              // Ex. 1800 MHz
        int mCpuCores;              // Ex. 4
        QString mUsername;          // Ex. marcus

        bool init;

        static SystemInfo *info;

        QList<long> lastTotals;
        QList<long> lastIdles;
};
